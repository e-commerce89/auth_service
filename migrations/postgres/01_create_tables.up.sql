CREATE TABLE "user_role" (
  "id" UUID PRIMARY KEY,
  "name" VARCHAR NOT NULL
);
CREATE TABLE "platform" (
  "id" UUID PRIMARY KEY,
  "name" VARCHAR
);
CREATE TABLE "user" (
  "id" UUID PRIMARY KEY,
  "name" VARCHAR NOT NULL,
  "email" VARCHAR NOT NULL,
  "login" VARCHAR,
  "phone" VARCHAR NOT NULL,
  "password" VARCHAR,
  "user_role_id" UUID NOT NULL REFERENCES "user_role" ("id"),
  "platform_id" UUID NOT NULL REFERENCES "platform" ("id"),
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE "permissions" (
  "id" UUID PRIMARY KEY,
  "name" VARCHAR
);
-- CREATE EXTENSION hstore;
CREATE TABLE "user_role_permissions" (
  "id" UUID PRIMARY KEY,
  "permission_id" UUID NOT NULL REFERENCES "permissions" ("id"),	
  "permission_name" VARCHAR NOT NULL,	
  "actions" json,
  "user_role_id" UUID NOT NULL REFERENCES "user_role" ("id")
);




