package postgres

import (
	"database/sql"

	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/pkg/helper"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage/repo"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) repo.UserRepoI {
	return userRepo{
		db: db,
	}
}

func (a userRepo) CreateUser(req *auth_service.CreateUserRequest) (string, error) {
	id := uuid.New().String()
	query := `INSERT INTO 
	"user" (
		"id",
		"name",
		"login",
		"phone",
		"email",
		"password",
		"user_role_id",
		"platform_id"
		) 
	VALUES (
		$1, 
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8
		)`
	_, err := a.db.Exec(query, id, req.Name, req.Login, req.Phone, req.Email, req.Password, req.UserRoleId, req.PlatformId)
	if err != nil {
		return "", err
	}
	return id, nil
}

func (a userRepo) GetUserList(req *auth_service.GetUserListRequest) (*auth_service.GetUserListResponse, error) {
	res := &auth_service.GetUserListResponse{}
	params := make(map[string]interface{})
	var arr []interface{}
	query := `SELECT
		"id",
		"name",
		"login",
		"phone",
		"email",
		"password",
		"user_role_id",
		"platform_id",
		"created_at",
		"updated_at"
	FROM
		"user"`
	filter := " WHERE 1=1"
	order := " ORDER BY created_at"
	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += " AND ((full_name || phone || login) ILIKE ('%' || :search || '%'))"
	}
	if req.Offset > 0 {
		params["offset"] = req.Offset
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "user"` + filter
	cQ, arr = helper.ReplaceQueryParams(cQ, params)
	err := a.db.QueryRow(cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + order + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := a.db.Query(q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &auth_service.User{}
		var updatedAt sql.NullString

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
			&obj.Login,
			&obj.Phone,
			&obj.Email,
			&obj.Password,
			&obj.UserRoleId,
			&obj.PlatformId,
			&obj.CreatedAt,
			&updatedAt,
		)

		if err != nil {
			return res, err
		}
		if updatedAt.Valid {
			obj.UpdatedAt = updatedAt.String
		}
		res.Users = append(res.Users, obj)
	}

	return res, nil
}

func (a userRepo) GetUserById(id string) (*auth_service.User, error) {
	res := &auth_service.User{}
	var updatedAt sql.NullString
	query := `SELECT
		"id",
		"name",
		"login",
		"phone",
		"email",
		"password",
		"user_role_id",
		"platform_id",
		"created_at",
		"updated_at"
	FROM
		"user"
	WHERE
		id = $1`

	err := a.db.QueryRow(query, id).Scan(
		&res.Id,
		&res.Name,
		&res.Login,
		&res.Phone,
		&res.Email,
		&res.Password,
		&res.UserRoleId,
		&res.PlatformId,
		&res.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return res, err
	}
	if updatedAt.Valid {
		res.UpdatedAt = updatedAt.String
	}

	return res, nil
}

func (a userRepo) UpdateUser(req *auth_service.UpdateUserRequest) (rowsAffected int64, err error) {
	query := `UPDATE "user" SET
		name = :name,
		phone = :phone,
		email = :email,
		login = :login,
		password = :password,
		user_role_id = :user_role_id,
		platform_id=:platform_id,
		updated_at = now()
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":           req.Id,
		"user_role_id": req.UserRoleId,
		"platform_id":  req.PlatformId,
		"name":         req.Name,
		"phone":        req.Phone,
		"email":        req.Email,
		"login":        req.Login,
		"password":     req.Password,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := a.db.Exec(q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected, _ = result.RowsAffected()

	return rowsAffected, err
}
func (a userRepo) DeleteUser(id string) (int64, error) {

	query := `DELETE FROM "user" WHERE id = $1`

	result, err := a.db.Exec(query, id)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()

	return rowsAffected, err
}
func (u userRepo) Register(req *auth_service.RegisterUserRequest) (string, error) {
	id := uuid.New().String()
	_, err := u.db.Exec(`INSERT INTO 
		"user" (
			id,
			name,
			login,
			phone,
			email,
			password,
			user_role_id,
			platform_id
			) 
		VALUES (
			$1, 
			$2,
			$3,
			$4,
			$5,
			$6,
			$7,
			$8
			)`,
		id,
		req.Name,
		req.Login,
		req.Phone,
		req.Email,
		req.Password,
		config.ClientId,
		config.PlatformId,
	)
	if err != nil {
		return "", err
	}
	return id, nil

}
func (u userRepo) GetUserByLogin(login string) (*auth_service.User, error) {
	user := &auth_service.User{}
	var updatedAt sql.NullString

	err := u.db.QueryRow(`
	SELECT 
		id,
		name,
		login,
		email,
		phone,
		password,
		user_role_id,
		platform_id,
		created_at,
		updated_at
		
	FROM "user"
	WHERE login=$1`, login).Scan(
		&user.Id,
		&user.Name,
		&user.Login,
		&user.Email,
		&user.Phone,
		&user.Password,
		&user.UserRoleId,
		&user.PlatformId,
		&user.CreatedAt,
		&updatedAt,
	)
	if updatedAt.Valid {
		user.UpdatedAt = updatedAt.String
	}
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return user, nil
}
