package postgres

import (
	"fmt"

	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/pkg/helper"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type userRoleRepo struct {
	db *sqlx.DB
}

func NewUserRoleRepo(db *sqlx.DB) repo.UserRoleRepoI {
	return userRoleRepo{
		db: db,
	}
}

func (a userRoleRepo) CreateUserRole(req *auth_service.CreateUserRole) (string, error) {
	fmt.Println("user role storage")
	id := uuid.New().String()
	query := `INSERT INTO 
	"user_role" (
		"id",
		"name"
		) 
	VALUES (
		$1, 
		$2
		)`
	_, err := a.db.Exec(query, id, req.Name)
	if err != nil {
		return "", err
	}
	return id, nil
}

func (a userRoleRepo) GetUserRoleList(req *auth_service.GetAllRequest) (*auth_service.UserRolesResponse, error) {
	res := &auth_service.UserRolesResponse{
		UserRoles: make([]*auth_service.UserRole, 0),
	}
	params := make(map[string]interface{})
	var arr []interface{}
	query := `SELECT
			"id",
			"name"
		FROM
			"user_role"`
	filter := " WHERE 1=1"
	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if req.Offset > 0 {
		params["offset"] = req.Offset
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "user_role"` + filter
	cQ, arr = helper.ReplaceQueryParams(cQ, params)
	err := a.db.QueryRow(cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := a.db.Query(q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &auth_service.UserRole{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
		)

		if err != nil {
			return res, err
		}
		res.UserRoles = append(res.UserRoles, obj)
	}

	return res, nil
}

func (a userRoleRepo) UpdateUserRole(req *auth_service.UserRole) (rowsAffected int64, err error) {
	query := `UPDATE "userRole" SET
		name = :name,
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":   req.Id,
		"name": req.Name,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := a.db.Exec(q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected, _ = result.RowsAffected()

	return rowsAffected, err
}
func (a userRoleRepo) DeleteUserRole(id string) (int64, error) {

	query := `DELETE FROM "user_role" WHERE id = $1`

	result, err := a.db.Exec(query, id)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()

	return rowsAffected, err
}
