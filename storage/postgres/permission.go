package postgres

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/pkg/helper"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type permissionRepo struct {
	db *sqlx.DB
}

func NewPermissionRepo(db *sqlx.DB) repo.PermissionRepoI {
	return permissionRepo{
		db: db,
	}
}

func (a permissionRepo) CreatePermission(req *auth_service.CreatePermissionRequest) (string, error) {
	id := uuid.New().String()
	query := `INSERT INTO 
	"permissions" (
		"id",
		"name"
		) 
	VALUES (
		$1, 
		$2
		)`
	_, err := a.db.Exec(query, id, req.Name)
	if err != nil {
		return "", err
	}
	return id, nil
}

func (a permissionRepo) GetPermissionList(req *auth_service.GetPermissionRequest) (*auth_service.PermissionsResponse, error) {
	res := &auth_service.PermissionsResponse{
		Permissions: make([]*auth_service.Permission, 0),
	}
	params := make(map[string]interface{})
	var arr []interface{}
	query := `SELECT
			"id",
			"name"
		FROM
			"permissions"`
	filter := " WHERE 1=1"
	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if req.Offset > 0 {
		params["offset"] = req.Offset
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "permissions"` + filter
	cQ, arr = helper.ReplaceQueryParams(cQ, params)
	err := a.db.QueryRow(cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := a.db.Query(q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &auth_service.Permission{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
		)

		if err != nil {
			return res, err
		}
		res.Permissions = append(res.Permissions, obj)
	}

	return res, nil
}

func (a permissionRepo) UpdatePermission(req *auth_service.Permission) (rowsAffected int64, err error) {
	query := `UPDATE "permission" SET
		name = :name,
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":   req.Id,
		"name": req.Name,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := a.db.Exec(q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected, _ = result.RowsAffected()

	return rowsAffected, err
}
func (a permissionRepo) DeletePermission(id string) (int64, error) {

	query := `DELETE FROM "permissions" WHERE id = $1`

	result, err := a.db.Exec(query, id)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()

	return rowsAffected, err
}
