package postgres

import (
	"encoding/json"
	"fmt"

	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type userRolePermissionRepo struct {
	db *sqlx.DB
}

func NewUserRolePermissionRepo(db *sqlx.DB) repo.UserRolePermissionRepoI {
	return userRolePermissionRepo{
		db: db,
	}
}

func (a userRolePermissionRepo) Create(req *auth_service.CreateUserRolePermission) (string, error) {
	jsonStr, err := json.Marshal(req.Actions)
	if err != nil {
		return "", err
	}
	id := uuid.New().String()
	query := `INSERT INTO 
	"user_role_permissions" (
		"id",
		"user_role_id",
		"permission_id",
		"permission_name",
		"actions"
		
		) 
	VALUES (
		$1, 
		$2,
		$3,
		$4,
		$5	
	
		)`
	fmt.Println(query)
	_, err = a.db.Exec(query, id, req.UserRoleId, req.PermissionId, req.PermissionName, jsonStr)
	if err != nil {
		return "", err
	}
	return id, nil
}

func (a userRolePermissionRepo) GetUserRolePermissionList(req *auth_service.GetUserRolePermissionRequest) (*auth_service.UserRolePermissionsResponse, error) {
	res := &auth_service.UserRolePermissionsResponse{
		UserRolePermissions: make([]*auth_service.UserRolePermission, 0),
	}
	query := `SELECT
		"id",
		"user_role_id",
		"permission_id",
		"permission_name",
		"actions"
	FROM
		"user_role_permissions"
	WHERE user_role_id=$1`

	cQ := `SELECT count(1) FROM "user_role_permissions"`
	err := a.db.QueryRow(cQ).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}
	rows, err := a.db.Query(query, req.UserRoleId)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &auth_service.UserRolePermission{}
		var action []uint8
		actions:=make(map[string]bool)
		err = rows.Scan(
			&obj.Id,
			&obj.UserRoleId,
			&obj.PermissionId,
			&obj.PermissionName,
			&action,
		)
		json.Unmarshal(action, &actions)
		obj.Actions=actions
		if err != nil {
			return res, err
		}
		res.UserRolePermissions = append(res.UserRolePermissions, obj)
	}
	return res, nil
}

func (a userRolePermissionRepo) DeleteUserRolePermisson(req *auth_service.DeleteUserRolePermissionRequest) (int64, error) {

	query := `DELETE FROM "user_role_permissions" WHERE id = $1 AND user_role_id=$2`

	result, err := a.db.Exec(query, req.Id, req.UserRoleId)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()

	return rowsAffected, err
}
