package storage

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage/postgres"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	User() repo.UserRepoI
	Permission() repo.PermissionRepoI
	UserRole() repo.UserRoleRepoI
	UserRolePermission() repo.UserRolePermissionRepoI
}
type storagePg struct {
	db   *sqlx.DB
	user repo.UserRepoI
	permission repo.PermissionRepoI
	userRole repo.UserRoleRepoI
	userRolePermission repo.UserRolePermissionRepoI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:   db,
		user: postgres.NewUserRepo(db),
		permission: postgres.NewPermissionRepo(db),
		userRolePermission: postgres.NewUserRolePermissionRepo(db),
		userRole: postgres.NewUserRoleRepo(db),
	}
}

func (p *storagePg) User() repo.UserRepoI {
	return p.user
}
func (p *storagePg) Permission() repo.PermissionRepoI {
	return p.permission
}
func (p *storagePg) UserRole() repo.UserRoleRepoI {
	return p.userRole
}

func (p *storagePg) UserRolePermission() repo.UserRolePermissionRepoI {
	return p.userRolePermission
}
