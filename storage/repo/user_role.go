package repo

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
)
type UserRoleRepoI interface {
	CreateUserRole(req *auth_service.CreateUserRole) (string, error)
	GetUserRoleList(req *auth_service.GetAllRequest) (*auth_service.UserRolesResponse, error)
	UpdateUserRole(req *auth_service.UserRole) (rowsAffected int64, err error)
	DeleteUserRole(id string) (int64, error) 
}