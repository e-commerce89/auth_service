package repo

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
)
type UserRolePermissionRepoI interface {
	Create(req *auth_service.CreateUserRolePermission) (string, error) 
	DeleteUserRolePermisson(req *auth_service.DeleteUserRolePermissionRequest) (int64, error)
	GetUserRolePermissionList(req *auth_service.GetUserRolePermissionRequest) (*auth_service.UserRolePermissionsResponse, error)
	
}