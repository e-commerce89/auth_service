package repo

import (
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
)
type PermissionRepoI interface {
	CreatePermission(req *auth_service.CreatePermissionRequest) (string, error)
	GetPermissionList(req *auth_service.GetPermissionRequest) (*auth_service.PermissionsResponse, error)
	UpdatePermission(req *auth_service.Permission) (rowsAffected int64, err error) 
	DeletePermission(id string) (int64, error) 
}