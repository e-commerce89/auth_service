package service

import (
	"context"

	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type userRoleService struct {
	log logger.LoggerI
	cfg config.Config
	auth_service.UnimplementedUserRoleServiceServer
	stg storage.StorageI
}

func NewUserRoleService(log logger.LoggerI, cfg config.Config, db *sqlx.DB) *userRoleService {
	return &userRoleService{
		log: log,
		cfg: cfg,
		stg: storage.NewStoragePg(db),
	}
}
func (s *userRoleService) Create(ctx context.Context, req *auth_service.CreateUserRole) (*auth_service.CreateUserRoleResponse, error) {
	s.log.Info("---CreateUserRole--->", logger.Any("req", req))

	pKey, err :=s.stg.UserRole().CreateUserRole(req)
	if err != nil {
		s.log.Error("!!!CreateUserRole--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &auth_service.CreateUserRoleResponse{Id: pKey}, nil

}

func (s *userRoleService) GetAll(ctx context.Context, req *auth_service.GetAllRequest) (*auth_service.UserRolesResponse, error) {
	s.log.Info("---GetUserRoleList--->", logger.Any("req", req))
	res, err := s.stg.UserRole().GetUserRoleList(req)

	if err != nil {
		s.log.Error("!!!GetUserRoleList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, err
}

func (s *userRoleService) Update(ctx context.Context, req *auth_service.UserRole) (*emptypb.Empty, error) {
	s.log.Info("---UpdateUserRole--->", logger.Any("req", req))

	rowsAffected, err := s.stg.UserRole().UpdateUserRole(req)

	if err != nil {
		s.log.Error("!!!UpdateUserRole--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.NotFound, "no rows were affected")
	}

	return &emptypb.Empty{}, err
}

func (s *userRoleService) Delete(ctx context.Context, req *auth_service.DeleteRequest) (*emptypb.Empty, error) {
	s.log.Info("---DeleteUserRole--->", logger.Any("req", req))

	rowsAffected, err := s.stg.UserRole().DeleteUserRole(req.Id)

	if err != nil {
		s.log.Error("!!!DeleteUserRole--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.NotFound, "no rows were affected")
	}

	return &emptypb.Empty{}, nil
}
