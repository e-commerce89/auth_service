package service

import (
	"context"

	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type permissionService struct {
	log logger.LoggerI
	cfg config.Config
	auth_service.UnimplementedPermissionServiceServer
	stg storage.StorageI
}

func NewPermissionService(log logger.LoggerI, cfg config.Config, db *sqlx.DB) *permissionService {
	return &permissionService{
		log: log,
		cfg: cfg,
		stg: storage.NewStoragePg(db),
	}
}
func (s *permissionService) Create(ctx context.Context, req *auth_service.CreatePermissionRequest) (*auth_service.CreatePermissionResponse, error) {
	s.log.Info("---CreatePermission--->", logger.Any("req", req))

	pKey, err := s.stg.Permission().CreatePermission(req)

	if err != nil {
		s.log.Error("!!!CreatePermission--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &auth_service.CreatePermissionResponse{Id: pKey}, nil

}

func (s *permissionService) GetAll(ctx context.Context, req *auth_service.GetPermissionRequest) (*auth_service.PermissionsResponse, error) {
	s.log.Info("---GetPermissionList--->", logger.Any("req", req))
	res, err := s.stg.Permission().GetPermissionList(req)

	if err != nil {
		s.log.Error("!!!GetPermissionList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, err
}

func (s *permissionService) Update(ctx context.Context, req *auth_service.Permission) (*emptypb.Empty, error) {
	s.log.Info("---UpdatePermission--->", logger.Any("req", req))

	rowsAffected, err := s.stg.Permission().UpdatePermission(req)

	if err != nil {
		s.log.Error("!!!UpdatePermission--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.NotFound, "no rows were affected")
	}

	return &emptypb.Empty{}, err
}

func (s *permissionService) Delete(ctx context.Context, req *auth_service.DeletePermissionRequest) (*emptypb.Empty, error) {
	s.log.Info("---DeletePermission--->", logger.Any("req", req))

	rowsAffected, err := s.stg.Permission().DeletePermission(req.Id)

	if err != nil {
		s.log.Error("!!!DeletePermission--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.NotFound, "no rows were affected")
	}

	return &emptypb.Empty{}, nil
}
