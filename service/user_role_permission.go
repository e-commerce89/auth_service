package service

import (
	"context"

	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/genproto/auth_service"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/auth_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type userRolePermissionService struct {
	log logger.LoggerI
	cfg config.Config
	auth_service.UnimplementedUserRolePermissionServiceServer
	stg storage.StorageI
}

func NewUserRolePermissionService(log logger.LoggerI, cfg config.Config, db *sqlx.DB) *userRolePermissionService {
	return &userRolePermissionService{
		log: log,
		cfg: cfg,
		stg: storage.NewStoragePg(db),
	}
}
func (s *userRolePermissionService) Create(ctx context.Context, req *auth_service.CreateUserRolePermission) (*auth_service.UserRolePermissionResponse, error) {
	s.log.Info("---CreateUserRolePermission--->", logger.Any("req", req))

	pKey, err := s.stg.UserRolePermission().Create(req)

	if err != nil {
		s.log.Error("!!!CreateUserRolePermission--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &auth_service.UserRolePermissionResponse{Id: pKey}, nil

}

func (s *userRolePermissionService) GetList(ctx context.Context, req *auth_service.GetUserRolePermissionRequest) (*auth_service.UserRolePermissionsResponse, error) {
	s.log.Info("---GetUserRolePermissionList--->", logger.Any("req", req))
	res, err := s.stg.UserRolePermission().GetUserRolePermissionList(req)

	if err != nil {
		s.log.Error("!!!GetUserRolePermissionList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, err
}

func (s *userRolePermissionService) Delete(ctx context.Context, req *auth_service.DeleteUserRolePermissionRequest) (*emptypb.Empty, error) {
	s.log.Info("---DeleteUserRolePermission--->", logger.Any("req", req))

	rowsAffected, err := s.stg.UserRolePermission().DeleteUserRolePermisson(req)

	if err != nil {
		s.log.Error("!!!DeleteUserRolePermission--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.NotFound, "no rows were affected")
	}

	return &emptypb.Empty{}, nil
}
